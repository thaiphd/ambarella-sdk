##
## ambarella/oryx/stream/AmbaConfig
##
## History:
##    2014/06/23 - [Louis Sun] Create
##
## Copyright (c) 2016 Ambarella, Inc.
##
## This file and its contents ("Software") are protected by intellectual
## property rights including, without limitation, U.S. and/or foreign
## copyrights. This Software is also the confidential and proprietary
## information of Ambarella, Inc. and its licensors. You may not use, reproduce,
## disclose, distribute, modify, or otherwise prepare derivative works of this
## Software or any portion thereof except pursuant to a signed license agreement
## or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
## In the absence of such an agreement, you agree to promptly notify and return
## this Software to Ambarella, Inc.
##
## THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
## INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
## MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
## IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
## INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
## (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
## LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##


config BUILD_AMBARELLA_ORYX_STREAM
  bool "Build Oryx Stream"
  depends on BUILD_AMBARELLA_ORYX_MW
  select BUILD_AMBARELLA_ORYX_UTIL
  select BUILD_AMBARELLA_ORYX_CONFIG
  select BUILD_AMBARELLA_ORYX_OSAL
  select CONFIG_AMBARELLA_LIBG7XX_SUPPORT
  select CONFIG_AMBARELLA_LIBOGG_SUPPORT
  select CONFIG_AMBARELLA_LIBOPUS_SUPPORT
  select CONFIG_AMBARELLA_SPEEX_SUPPORT
  default n
  help
    Build Oryx Stream

config BUILD_AMBARELLA_ORYX_STREAM_RECORD
  bool "Build Oryx Stream Record Module"
  depends on BUILD_AMBARELLA_ORYX_STREAM
  select BUILD_AMBARELLA_ORYX_AUDIO
  select BUILD_AMBARELLA_ORYX_AUDIO_DEVICE
  select BUILD_AMBARELLA_ORYX_AUDIO_CAPTURE
  select BUILD_AMBARELLA_ORYX_AUDIO_CAPTURE_PULSE
  default y
  help
    Build Oryx Stream Record Module

config BUILD_AMBARELLA_ORYX_STREAM_PLAYBACK
  bool "Build Oryx Stream Playback Module"
  depends on BUILD_AMBARELLA_ORYX_STREAM
  default y
  help
    Build Oryx Stream Playback Module

config BUILD_AMBARELLA_ORYX_STREAM_UNIT_TEST
  bool "Build Oryx Stream Module Unit Test Program"
  depends on BUILD_AMBARELLA_ORYX_STREAM
  default n
  help
    Build Oryx Stream Module Unit Test Program

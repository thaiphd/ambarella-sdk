/*
 * am_api_video.h
 *
 *  History:
 *    Nov 18, 2014 - [Shupeng Ren] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*! @file am_api_video.h
 *  @brief This file defines Oryx Video Services related data structures
 */
#ifndef _AM_API_VIDEO_H_
#define _AM_API_VIDEO_H_

#include "commands/am_api_cmd_video.h"

// Setting item masks
/*! @defgroup airapi-datastructure-video Data Structure of Video Service
 *  @ingroup airapi-datastructure
 *  @brief All Oryx Video Service related method call data structures
 *  @{
 */

/*
 * Feature
 */
/*****************************Feature Parameters*******************************/
/*! @defgroup airapi-datastructure-video-feature feature Parameters
 *  @ingroup airapi-datastructure-video
 *  @brief feature related parameters,
 *         refer to @ref airapi-commandid-video-feature "feature Commands" to
 *         see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_FEATURE_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_FEATURE_SET
 *  @{
 *//* Start of feature Parameter Section */

/*! @enum AM_FEATURE_CONFIG_BITS
 *  @brief feature configuration to indicate which config needs to be modified
 */
enum AM_FEATURE_CONFIG_BITS
{
  AM_FEATURE_CONFIG_MODE_EN_BIT        = 0,//!< Change feature mode
  AM_FEATURE_CONFIG_HDR_EN_BIT         = 1,//!< Change feature hdr
  AM_FEATURE_CONFIG_ISO_EN_BIT         = 2,//!< Change feature iso
  AM_FEATURE_CONFIG_DEWARP_EN_BIT      = 3,//!< Change feature dewarp
  AM_FEATURE_CONFIG_DPTZ_EN_BIT        = 4,//!< Change feature dptz
  AM_FEATURE_CONFIG_BITRATECTRL_EN_BIT = 5,//!< Change feature bitrate ctrl
  AM_FEATURE_CONFIG_OVERLAY_EN_BIT     = 6,//!< Change feature overlay
};


/*! @struct am_feature_config_s
 *  @brief For feature config file GET and SET
 */
/*! @typedef am_feature_config_t
 *  @brief For feature config file GET and SET
 */
typedef struct am_feature_config_s
{
    /*!
     * @sa AM_FEATURE_CONFIG_BITS
     */
    uint32_t enable_bits; //!< AM_FEATURE_CONFIG_BITS

    /*!
     * - iav_version
     */
    uint32_t version;

    /*!
     * - 0: auto
     * - 1: mode0
     * - 2: mode1
     * - 3: mode4
     * - 4: mode5
     */
    uint32_t mode;

    /*!
     * - 0: none
     * - 1: 2x
     * - 2: 3x
     * - 3: 4x
     * - 4: sensor
     */
    uint32_t hdr;

    /*!
     * - 0: normal
     * - 1: plus
     * - 2: advanced
     */
    uint32_t iso;

    /*!
     * - 0: none
     * - 1: ldc
     * - 2: full
     */
    uint32_t dewarp_func;

    /*!
     * - 0: none
     * - 1: enable
     */
    uint32_t dptz;

    /*!
     * - 0: none
     * - 1: lbr
     */
   uint32_t bitrate_ctrl;

    /*!
     * - 0: none
     * - 1: enable
     */
   uint32_t overlay;


} am_feature_config_t;



/*! @} */ /* End of feature Parameters */
/******************************************************************************/


/*
 * VIN
 */
/******************************VIN Parameters**********************************/
/*! @defgroup airapi-datastructure-video-vin VIN Parameters
 *  @ingroup airapi-datastructure-video
 *  @brief VIN related parameters,
 *         refer to @ref airapi-commandid-video-vin "VIN Commands" to see
 *         related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_VIN_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_VIN_SET
 *  @{
 *//* Start of VIN Parameter Section */

/*! @enum AM_VIN_CONFIG_BITS
 *  @brief VIN configuration type to indicate which config needs to be modified
 */
enum AM_VIN_CONFIG_BITS
{
  AM_VIN_CONFIG_WIDTH_HEIGHT_EN_BIT  = 0,//!< Change VIN Width and Height
  AM_VIN_CONFIG_FLIP_EN_BIT          = 1,//!< Change VIN Flip config
  AM_VIN_CONFIG_HDR_MODE_EN_BIT      = 2,//!< Change VIN HDR mode
  AM_VIN_CONFIG_FPS_EN_BIT           = 3,//!< Change VIN FPS config
  AM_VIN_CONFIG_BAYER_PATTERN_EN_BIT = 4,//!< Change VIN bayer pattern
};

/*! @struct am_vin_config_s
 *  @brief For VIN config file GET and SET
 */
/*! @typedef am_vin_config_t
 *  @brief For VIN config file GET and SET
 */
typedef struct am_vin_config_s
{
    /*!
     * @sa AM_VIN_CONFIG_BITS
     */
    uint32_t enable_bits; //!< AM_VIN_CONFIG_BITS

    uint32_t vin_id; //!< VIN ID

    uint32_t width; //!< VIN Width
    uint32_t height; //!< VIN Height

    /*!
     * - 0:   not flip
     * - 1:   flipv
     * - 2:   fliph
     * - 3:   flip both V & h
     * - 255: auto
     */
    uint8_t flip; //!< VIN flip

    /*!
     * - 0: single exposure, no HDR
     * - 1: 2x hdr exposure
     * - 2: 3x hdr exposure
     * - 3: 4x hdr exposure
     */
    uint8_t hdr_mode; //!< VIN HDR Mode
    uint16_t reserved0; //!< Reserved space to keep align

    /*!
     * - 0:     auto
     * - x:     x
     * - 1000:   29.97
     * - 1001:   59.94
     * - 1002:   23.976
     * - 1003:   12.5
     * - 1004:   6.25
     * - 1005:   3.125
     * - 1006:   7.5
     * - 1007:   3.75
     */
    uint16_t fps; //!< VIN FPS

    /*!
     * - 0: auto
     * - 1: RG
     * - 2: BH
     * - 3: GR
     * - 4: GB
     */
    uint8_t bayer_pattern; //!< VIN bayer pattern
    uint8_t reserved1; //!< Reserved space to keep align
} am_vin_config_t;

/*! @struct am_vin_info_s
 *  @brief For dynamically GET VIN info
 */
/*! @typedef am_vin_info_t
 *  @brief For dynamically GET VIN info
 */
typedef struct am_vin_info_s {
    uint32_t  width;  //!< VIN width
    uint32_t  height; //!< VIN height
    uint32_t  vin_id; //!< VIN ID

    /*!
     * - 0:     auto
     * - x:     x
     * - 1000:   29.97
     * - 1001:   59.94
     * - 1002:   23.976
     * - 1003:   12.5
     * - 1004:   6.25
     * - 1005:   3.125
     * - 1006:   7.5
     * - 1007:   3.75
     */
    uint16_t   max_fps; //!< VIN fps
    uint16_t   fps;     //!< The same as max_fps

    /*!
     * - 0: auto
     * - 1: Progressive
     * - 2: Interlace
     */
    uint8_t   format; //!< VIN format

    /*!
     * - 0: AUTO
     * - 1: YUV BT601
     * - 2: YUV BT656
     * - 3: YUV BT1120
     * - 4: RGB BT601
     * - 5: RGB BT656
     * - 6: RGB RAW
     * - 7: RGB BT1120
     */
    uint8_t   type;  //!< VIN type

    /*!
     * - 0: auto
     * - X: X bits
     */
    uint8_t   bits; //!< VIN bits

    /*!
     * - 0: auto
     * - 1: 4:3
     * - 2: 16:9
     */
    uint8_t   ratio; //!< VIN aspect ratio

    /*!
     * - 0: auto
     * - 1: NTSC
     * - 2: PAL
     * - 3: SECAM
     * - 4: ALL
     */
    uint8_t   system; //!< VIN system

    /*!
     * - 0: none
     * - 1: auto
     * - 2: FLIP_V
     * - 3: FLIP_H
     * - 4: FLIP both V & H
     */
    uint8_t   flip; //!< VIN flip type

    /*!
     * - 0: linear mode
     * - 1: 2x
     * - 2: 3x
     * - 3: 4x
     */
    uint8_t   hdr_mode; //!< VIN HDR mode
    uint8_t   reserved; //!< Reserved place to keep align
} am_vin_info_t;

/*! @} */ /* End of VIN Parameters */
/******************************************************************************/

/*! @defgroup airapi-datastructure-video-vout VOUT Parameters
 *  @ingroup airapi-datastructure-video
 *  @brief VOUT related parameters,
 *         refer to @ref airapi-commandid-video-vout "VOUT Commands" to see
 *         related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT
 *  @sa AM_IPC_MW_CMD_VIDEO_CFG_VOUT_SET
 *  @sa AM_IPC_MW_CMD_VIDEO_CFG_VOUT_GET
 *  @{
 *//* Start of VOUT Parameter Section */

/*! @enum AM_VOUT_CONFIG_BITS
 *  @brief VOUT configuration type to indicate which config needs to be modified
 */
enum AM_VOUT_CONFIG_BITS
{
  AM_VOUT_CONFIG_TYPE_EN_BIT          = 0,//!< Change VOUT type
  AM_VOUT_CONFIG_VIDEO_TYPE_EN_BIT    = 1,//!< Change VOUT video type
  AM_VOUT_CONFIG_MODE_EN_BIT          = 2,//!< Change VOUT mode
  AM_VOUT_CONFIG_FLIP_EN_BIT          = 3,//!< Change VOUT flip
  AM_VOUT_CONFIG_ROTATE_EN_BIT        = 4,//!< Change VOUT rotate
  AM_VOUT_CONFIG_FPS_EN_BIT           = 5,//!< Change VOUT FPS
};

/*! @macros VOUT_MAX_CHAR_NUM
 *  @brief VOUT max char number
 */
#define VOUT_MAX_CHAR_NUM  (16)  //!< used to vout mode

/*! @struct am_vout_config_s
 *  @brief For VOUT config file GET and SET
 */
/*! @typedef am_vout_config_t
 *  @brief For VOUT config file GET and SET
 */
typedef struct am_vout_config_s
{
    /*!
     * @sa AM_VOUT_CONFIG_BITS
     */
    uint32_t enable_bits; //!< AM_VOUT_CONFIG_BITS

    uint32_t vout_id; //!< VOUT ID

    /*!
     * - 0:   none
     * - 1:   CVBS
     * - 2:   HDMI
     * - 3:   LCD
     * - 4:   YPbPr
     */
    uint32_t type; //!< VOUT type

    /*!
     * - 0:   none
     * - 1:   YUV BT601
     * - 2:   YUV BT656
     * - 3:   YUV BT1120
     * - 4:   RGB BT601
     * - 5:   RGB BT656
     * - 6:   RGB BT1120
     */
    uint32_t video_type; //!< VOUT video type

    /*!
     * - 0:   not flip
     * - 1:   flipv
     * - 2:   fliph
     * - 3:   flip both V & h
     */
    uint32_t flip; //!< VOUT flip

    /*!
     * - 0:   not rotate
     * - 1:   rotate 90 degree
     */
    uint32_t rotate; //!< VOUT rotate

    /*!
     * - 0:     auto
     * - x:     x
     * - 1000:   29.97
     * - 1001:   59.94
     * - 1002:   23.976
     * - 1003:   12.5
     * - 1004:   6.25
     * - 1005:   3.125
     * - 1006:   7.5
     * - 1007:   3.75
     */
    uint32_t fps; //!< VOUT FPS

    /*!
     * vout mode to use
     */
    char mode[VOUT_MAX_CHAR_NUM];
} am_vout_config_t;

/*! @} */ /* End of VOUT Parameters */
/******************************************************************************/

/*
 * Stream Format
 */
/***************************Stream Format Parameters***************************/
/*! @defgroup airapi-datastructure-video-stream-fmt Stream Format Parameters
 *  @ingroup airapi-datastructure-video
 *  @brief Stream format related parameters,
 *         refer to @ref airapi-commandid-video-stream-fmt
 *         "Stream Format Commands" to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_STREAM_FMT_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_STREAM_FMT_SET
 *  @{
 */ /* Start of Stream Format Parameters */

/*! @enum AM_STREAM_FMT_BITS
 *  @brief Stream format type to indicate which config needs to be modified
 */
enum AM_STREAM_FMT_BITS {
  AM_STREAM_FMT_ENABLE_EN_BIT       = 0,  //!< Bit0
  AM_STREAM_FMT_TYPE_EN_BIT         = 1,  //!< Bit1
  AM_STREAM_FMT_SOURCE_EN_BIT       = 2,  //!< Bit2
  AM_STREAM_FMT_FRAME_NUM_EN_BIT    = 3,  //!< Bit3
  AM_STREAM_FMT_FRAME_DEN_EN_BIT    = 4,  //!< Bit4
  AM_STREAM_FMT_WIDTH_EN_BIT        = 5,  //!< Bit5
  AM_STREAM_FMT_HEIGHT_EN_BIT       = 6,  //!< Bit6
  AM_STREAM_FMT_OFFSET_X_EN_BIT     = 7,  //!< Bit7
  AM_STREAM_FMT_OFFSET_Y_EN_BIT     = 8,  //!< Bit8
  AM_STREAM_FMT_HFLIP_EN_BIT        = 9,  //!< Bit9
  AM_STREAM_FMT_VFLIP_EN_BIT        = 10, //!< Bit10
  AM_STREAM_FMT_ROTATE_EN_BIT       = 11, //!< Bit11
};

/*! @struct am_stream_fmt_s
 *  @brief For stream format file GET and SET
 */
/*! @typedef am_stream_fmt_t
 *  @brief For stream format file GET and SET
 */
typedef struct am_stream_fmt_s {
    /*!
     * @sa AM_STREAM_FMT_BITS
     */
    uint32_t  enable_bits;

    /*!
     * Video stream ID, start from 0
     */
    uint32_t  stream_id;

    /*!
     * - 0: Disable
     * - 1: Enable
     */
    uint32_t  enable;

    /*!
     * - 0: None
     * - 1: H264
     * - 2: H265
     * - 3: MJPEG
     */
    uint32_t  type;

    /*!
     * - 0: Main buffer
     * - 1: 2nd buffer
     * - 2: 3rd buffer
     * - 3: 4th buffer
     * - 4: 5th buffer
     * - 5: PMN buffer
     * - 6: EFM buffer
     */
    uint32_t  source;

    /*!
     * Frame factor: frame_fact_num/frame_fact_den, for example 1/2, 1/1
     * frame_fact_num must be no bigger than frame_fact_den
     */
    uint32_t  frame_fact_num;

    /*!
     * Frame factor: frame_fact_num/frame_fact_den, for example 1/2, 1/1
     * frame_fact_num must be no bigger than frame_fact_den
     */
    uint32_t  frame_fact_den;

    /*!
     * Video width.
     * multiple of 16, -1 for auto configure
     */
    uint32_t  width;

    /*!
     * Video height.
     * multiple of 8, -1 for auto configure
     */
    uint32_t  height;

    /*!
     * Video offset x.
     * multiple of 4
     */
    uint32_t  offset_x;

    /*!
     * Video offset y.
     * multiple of 4
     */
    uint32_t  offset_y;

    /*! horizontal flip.
     * - 0: disable hflip
     * - 1: enable hflip
     */
    uint32_t  hflip;

    /*! vertical flip.
     * - 0: disable vflip
     * - 1: enable vflip
     */
    uint32_t  vflip;

    /*!
     * - 0: disable rotate
     * - 1: enable rotate
     */
    uint32_t  rotate;
} am_stream_fmt_t;

/*! @} */ /* End of Stream Format Paramaters */
/******************************************************************************/

/*
 * Stream Config
 */
/********************************Stream Config*********************************/
/*! @defgroup airapi-datastructure-video-stream-cfg Stream Config Parameters
 *  @ingroup airapi-datastructure-video
 *  @brief Stream config related parameters,
 *         refer to @ref airapi-commandid-video-stream-cfg
 *         "Stream Config Commands" to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_STREAM_CFG_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_STREAM_CFG_SET
 *  @{
 */ /* Start of Stream Config Parameters */

/*! @enum AM_H264_CFG_BITS
 *  @brief H264 config type to indicate which config needs to be modified
 */
enum AM_H264_CFG_BITS {
  AM_H264_CFG_BITRATE_CTRL_EN_BIT         = 0,
  AM_H264_CFG_PROFILE_EN_BIT              = 1,
  AM_H264_CFG_AU_TYPE_EN_BIT              = 2,
  AM_H264_CFG_CHROMA_EN_BIT               = 3,
  AM_H264_CFG_M_EN_BIT                    = 4,
  AM_H264_CFG_N_EN_BIT                    = 5,
  AM_H264_CFG_IDR_EN_BIT                  = 6,
  AM_H264_CFG_BITRATE_EN_BIT              = 7,
  AM_H264_CFG_MV_THRESHOLD_EN_BIT         = 8,
  AM_H264_CFG_FLAT_AREA_IMPROVE_EN_BIT    = 9,
  AM_H264_CFG_MULTI_REF_P_EN_BIT          = 10,
  AM_H264_CFG_FAST_SEEK_INTVL_EN_BIT      = 11,
};

/*! @struct am_h264_cfg_s
 *  @brief For h264 config file GET and SET
 */
/*! @typedef am_h264_cfg_t
 *  @brief For h264 config file GET and SET
 */
typedef struct am_h264_cfg_s {
    /*!
     * @sa AM_H264_CFG_BITS
     */
    uint32_t    enable_bits;

    /*!
     * Video stream ID, start from 0
     */
    uint32_t    stream_id;

    /*!
     * - 0: CBR
     * - 1: VBR
     * - 2: CBR QUALITY
     * - 3: VBR QUALITY
     * - 4: CBR2
     * - 5: VBR2
     * - 6: LBR
     */
    uint32_t    bitrate_ctrl;

    /*!
     * - 0: Baseline
     * - 1: Main
     * - 2: High
     */
    uint32_t    profile;

    /*!
     * - 0: NO_AUD_NO_SEI
     * - 1: AUD_BEFORE_SPS_WITH_SEI
     * - 2: AUD_AFTER_SPS_WITH_SEI
     * - 3: NO_AUD_WITH_SEI
     */
    uint32_t    au_type;

    /*!
     * - 0: 420
     * - 1: 422
     * - 2: Mono
     */
    uint32_t    chroma;

    /*!
     * 1 ~ 3
     */
    uint32_t    M;

    /*!
     * 15 ~ 1800
     */
    uint32_t    N;

    /*!
     * 1 ~ 4
     */
    uint32_t    idr_interval;

    /*!
     * bit per second@30FPS
     * 6400 ~ 12000000
     */
    uint32_t    target_bitrate;

    /*!
     * 0 ~ 64
     */
    uint32_t    mv_threshold;

    /*! flat area improve
     * - 1: enable
     * - 0: disable
     */
    uint32_t    flat_area_improve;

    /*!
     * enable multi-reference P frame
     *
     * - 1: enable
     * - 0: disable
     */
    uint32_t    multi_ref_p;

    /*!
     * specify fast seek P frame interval
     *
     * 0 ~ 63
     */
    uint32_t    fast_seek_intvl;
} am_h264_cfg_t;

/*! @enum AM_MJPEG_CFG_BITS
 *  @brief MJPEG config type to indicate which config needs to be modified
 */
enum AM_MJPEG_CFG_BITS {
  AM_MJPEG_CFG_QUALITY_EN_BIT    = 0,
  AM_MJPEG_CFG_CHROMA_EN_BIT     = 1,
};

/*! @struct am_mjpeg_cfg_s
 *  @brief For mjpeg config file GET and SET
 */
/*! @typedef am_mjpeg_cfg_t
 *  @brief For mjpeg config file GET and SET
 */
typedef struct am_mjpeg_cfg_s {
    /*!
     * @sa AM_MJPEG_CFG_BITS
     */
    uint32_t    enable_bits;

    /*!
     * Video stream ID, start from 0
     */
    uint32_t    stream_id;

    /*!
     * 1 ~ 99
     */
    uint32_t    quality;

    /*!
     * - 0: 420
     * - 1: 422
     * - 2: Mono
     */
    uint32_t    chroma;
} am_mjpeg_cfg_t;

/*! @enum AM_STREAM_CFG_BITS */
enum AM_STREAM_CFG_BITS
{
  AM_STREAM_CFG_H264_EN_BIT = 0, //!< Config stream to H.264
  AM_STREAM_CFG_MJPEG_EN_BIT = 1, //!< Config stream to MJPEG
};

/*! @struct am_stream_cfg_s
 *  @brief For stream config file GET and SET
 */
/*! @typedef am_stream_cfg_t
 *  @brief For stream config file GET and SET
 */
typedef struct am_stream_cfg_s {
    /*!
     * @sa AM_STREAM_CFG_BITS
     */
    uint32_t        enable_bits;

    uint32_t        stream_id;

    /*!
     * @sa am_h264_cfg_s
     */
    am_h264_cfg_t   h264;

    /*!
     * @sa am_mjpeg_cfg_s
     */
    am_mjpeg_cfg_t  mjpeg;
} am_stream_cfg_t;

/*! @enum AM_STREAM_OFFSET_BITS */
enum AM_STREAM_OFFSET_BITS
{
  AM_STREAM_OFFSET_X_EN_BIT = 0,
  AM_STREAM_OFFSET_Y_EN_BIT = 1,
};

/*! @struct am_stream_offset_s
 *  @brief For stream offset GET and SET
 */
/*! @typedef am_stream_offset_t
 *  @brief For stream offset GET and SET
 */
typedef struct am_stream_offset_s {
    /*!
     * @sa AM_STREAM_OFFSET_BITS
     */
    uint32_t        enable_bits;

    uint32_t        stream_id;

    uint32_t x;

    uint32_t y;
} am_stream_offset_t;


/*! @} */ /* End of Stream Config Parameters */
/******************************************************************************/

/*
 * Source buffer
 */
/******************************Source Buffer Format****************************/
/*! @defgroup airapi-datastructure-video-src-buf-fmt Source Buffer Format
 *  @ingroup airapi-datastructure-video
 *  @brief Source buffer format related parameters,
 *         refer to @ref airapi-commandid-video-src-buf-fmt
 *         "Source Buffer Format Commands" to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_SET
 *  @{
 */ /* Start of Source Buffer Format */

/*! @enum AM_BUFFER_FMT_BITS
 *  @brief Buffer format type to indicate which config needs to be modified
 */
enum AM_BUFFER_FMT_BITS {
  AM_BUFFER_FMT_TYPE_EN_BIT           = 0,
  AM_BUFFER_FMT_INPUT_CROP_EN_BIT     = 1,
  AM_BUFFER_FMT_INPUT_WIDTH_EN_BIT    = 2,
  AM_BUFFER_FMT_INPUT_HEIGHT_EN_BIT   = 3,
  AM_BUFFER_FMT_INPUT_X_EN_BIT        = 4,
  AM_BUFFER_FMT_INPUT_Y_EN_BIT        = 5,
  AM_BUFFER_FMT_WIDTH_EN_BIT          = 6,
  AM_BUFFER_FMT_HEIGHT_EN_BIT         = 7,
  AM_BUFFER_FMT_PREWARP_EN_BIT        = 8,
};

/*! @struct am_buffer_fmt_s
 *  @brief For buffer format file GET and SET
 */
/*! @typedef am_buffer_fmt_t
 *  @brief For buffer format file GET and SET
 */
typedef struct am_buffer_fmt_s {
    /*!
     * @sa AM_BUFFER_FMT_BITS
     */
    uint32_t    enable_bits;

    /*!
     * - 0: Main buffer
     * - 1: 2nd buffer
     * - 2: 3rd buffer
     * - 3: 4th buffer
     */
    uint32_t    buffer_id;

    /*!
     * - 0: off
     * - 1: encode
     * - 2: preview
     */
    uint32_t    type;

    /*!
     * - 0: false
     * - 1: true
     */
    uint32_t    input_crop;

    /*!
     * input window:
     * {input_width, input_height, input_offset_x, input_offset_y}
     * Only use in input_crop is true
     */
    uint32_t    input_width;

    /*!
     * input window:
     * {input_width, input_height, input_offset_x, input_offset_y}
     * Only use in input_crop is true
     */
    uint32_t    input_height;

    /*!
     * input window:
     * {input_width, input_height, input_offset_x, input_offset_y}
     * Only use in input_crop is true
     */
    uint32_t    input_offset_x;

    /*!
     * input window:
     * {input_width, input_height, input_offset_x, input_offset_y}
     * Only use in input_crop is true
     */
    uint32_t    input_offset_y;

    /*!
     * buffer size: (width, height)
     */
    uint32_t    width;

    /*!
     * buffer size: (width, height)
     */
    uint32_t    height;

    /*!
     * - 0: false
     * - 1: true
     */
    uint32_t    prewarp;
} am_buffer_fmt_t;
/*! @} */ /* End of Source Buffer Format */
/******************************************************************************/

/*
 * Source buffer allocate style
 */
/*****************************Buffer Allocation Style**************************/
/*! @defgroup airapi-datastructure-video-src-buf-style Buffer Allocation Style
 *  @ingroup airapi-datastructure-video
 *  @brief Buffer allocation style related parameters,
 *         refer to @ref airapi-commandid-video-buf-style
 *         "Buffer Allocation Style Commands" to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_SET
 *  @{
 */ /* Start of Buffer Allocation Style */

/*! @struct am_buffer_alloc_style_s
 *  @brief For buffer allocation style GET and SET
 */
/*! @typedef am_buffer_alloc_style_t
 *  @brief For buffer allocation style GET and SET
 */
typedef struct am_buffer_alloc_style_s {
    /*!
     * - 0: manual
     * - 1: auto
     */
    uint32_t    alloc_style;
} am_buffer_alloc_style_t;

/*! @struct am_stream_status_s
 *  @brief For stream status GET
 */
/*! @typedef am_stream_status_t
 *  @brief For stream status GET
 */
typedef struct am_stream_status_s {
    /*!
     * bit OR'ed of all streams,
     * stream[0] is at bit 0, stream[x] is at bit x,
     * bit = 1 means stream is encoding,
     * otherwise it's not encoding.
     */
    uint32_t status;
} am_stream_status_t;
/*! @} */ /* End of Buffer Allocation Style */
/******************************************************************************/

/*
 * DPTZ
 */
/********************************DPTZ *****************************************/
/*! @defgroup airapi-datastructure-video-dptz DPTZ
 *  @ingroup airapi-datastruture-video
 *  @brief DPTZ Warp related parameters used,
 *         refer to @ref airapi-commandid-video-dptz "DPTZ Commands"
 *         to see related commands.
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_SET
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET
 *  @{
 */ /* Start of DPTZ */

/*! @enum AM_DPTZ_BITS
 *  @brief DPTZ type to indicate which configure needs to be modified
 */
enum AM_DPTZ_BITS {
  AM_DPTZ_PAN_RATIO_EN_BIT    = 0,
  AM_DPTZ_TILT_RATIO_EN_BIT   = 1,
  AM_DPTZ_ZOOM_RATIO_EN_BIT   = 2,
  AM_DPTZ_SIZE_X_EN_BIT       = 3,
  AM_DPTZ_SIZE_Y_EN_BIT       = 4,
  AM_DPTZ_SIZE_W_EN_BIT       = 5,
  AM_DPTZ_SIZE_H_EN_BIT       = 6
};

/*! @typedef am_dptz_ratio_t
 *  @brief For DPTZ RATIO GET and SET
 */
typedef struct am_dptz_ratio_s {
    /*!
     * @sa AM_DPTZ_BITS
     */
    uint32_t enable_bits;
    /*!
     * 0~3, 0: main buffer (DPTZ-I),  1-3: sub buffer (DPTZ-II)
     * */
    uint32_t buffer_id;
    /*!
     * -1.0~1.0, -1.0: left most,  1.0: right most
     * */
    float pan_ratio;
    /*!
     * -1.0~1.0, -1.0: top most,  1.0: bottom most
     * */
    float tilt_ratio;
    /*!
     * 0.1~8.0, 0.1: zoom in most, 1: original, 8.0: zoom out most
     */
    float zoom_ratio;
} am_dptz_ratio_t;

/*! @typedef am_dptz_size_t
 *  @brief For DPTZ Size GET and SET
 */
typedef struct am_dptz_size_s {
    /*!
     * @sa AM_DPTZ_SIZE_BITS
     */
    uint32_t enable_bits;
    /*!
     * 0~3, 0: main buffer (DPTZ-I),  1-3: sub buffer (DPTZ-II)
     * */
    uint32_t buffer_id;

    uint32_t w;//!< input window width
    uint32_t h;//!< input window height
    uint32_t x;//!< input window x
    uint32_t y;//!< input window y

} am_dptz_size_t;

/*! @} */ /* End of DPTZ */
/******************************************************************************/

/*
 * Warp
 */
/********************************Warp *****************************************/
/*! @defgroup airapi-datastructure-video-warp Warp
 *  @ingroup airapi-datastruture-video
 *  @brief DPTZ Warp related parameters used,
 *         refer to @ref airapi-commandid-video-warp "Warp Commands"
 *         to see related commands.
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_WARP_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_WARP_SET
 *  @{
 */ /* Start of Warp */

/*! @enum AM_WARP_BITS
 *  @brief Warp type to indicate which configure needs to be modified
 */
enum AM_WARP_BITS {
  AM_WARP_LDC_STRENGTH_EN_BIT     = 0,
  AM_WARP_PANO_HFOV_DEGREE_EN_BIT = 1,
  AM_WARP_REGION_YAW_EN_BIT       = 2,
};

/*! @typedef am_warp_t
 *  @brief For Warp GET and SET
 */
typedef struct am_warp_s {
    /*!
     * @sa AM_WARP_BITS
     */
    uint32_t enable_bits;
    /*!
     * 0.0~20.0, 0.0: do not do lens distortion calibration,
     *  20: maximum lens distortion calibration
     */
    float ldc_strength;
    /*!
     * 1.0~180.0, only for panorama mode
     */
    float pano_hfov_degree;
    /*!
     * -90~90, Lens Yaw in degree
     */
    int warp_region_yaw;
} am_warp_t;

/*! @} */ /* End of Warp */
/******************************************************************************/

/*
 * H.264 encode low bite rate control
 */
/********************************H264 LBR Control******************************/
/*! @defgroup airapi-datastructure-video-lbr H.264 LBR Control
 *  @ingroup airapi-datastructure-video
 *  @brief H.264 LBR control related parameters used,
 *         refer to @ref airapi-commandid-video-lbr "H.264 LBR Control Commands"
 *         to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_SET
 *  @{
 */ /* Start of H264 LBR Control */

/*! @enum AM_ENCODE_H264_LBR_CTRL_BITS
 *  @brief H.264 LBR control type to indicate which config needs to be modified
 */
enum AM_ENCODE_H264_LBR_CTRL_BITS {
  AM_ENCODE_H264_LBR_ENABLE_LBR_EN_BIT              = 0,
  AM_ENCODE_H264_LBR_AUTO_BITRATE_CEILING_EN_BIT    = 1,
  AM_ENCODE_H264_LBR_BITRATE_CEILING_EN_BIT         = 2,
  AM_ENCODE_H264_LBR_DROP_FRAME_EN_BIT              = 3,
};

/*! @struct am_encode_h264_lbr_ctrl_s
 *  @brief For h264 lbr control GET and SET
 */
/*! @typedef am_encode_h264_lbr_ctrl_t
 *  @brief For h264 lbr control GET and SET
 */
typedef struct am_encode_h264_lbr_ctrl_s {
    /*!
     * @sa AM_ENCODE_H264_LBR_CTRL_BITS
     */
    uint32_t enable_bits;
    uint32_t stream_id;

    /*!
     * - 0: disable LBR and use CBR
     * - 1: enable LBR
     */
    bool enable_lbr;

    /*!
     * - false: close auto bitrate ceiling and need to set manually
     * - true: auto bitrate ceiling, can not set bitrate ceiling now.
     */
    bool auto_bitrate_ceiling;

    /*!
     * when auto_bitrate_seiling is false, you can set it.
     */
    uint32_t bitrate_ceiling;

    /*!
     * - false: do not drop frame when motion is large
     * - true: drop frame auto when motion is large
     */
    bool drop_frame;
} am_encode_h264_lbr_ctrl_t;
/*! @} */ /* End of H264 LBR Control */
/******************************************************************************/

/*
 * EIS mode control
 */
/********************************EIS mode Control******************************/
/*! @defgroup airapi-datastructure-video-EIS EIS Control
 *  @ingroup airapi-datastructure-video
 *  @brief EIS control related parameters used,
 *         refer to @ref airapi-commandid-video-eis "EIS Control Commands"
 *         to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_ENCODE_EIS_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_ENCODE_EIS_SET
 *  @{
 */ /* Start of EIS Control */

/*! @enum AM_ENCODE_EIS_CTRL_BITS
 *  @brief EIS control type to indicate which config needs to be modified
 */
enum AM_ENCODE_EIS_CTRL_BITS {
  AM_ENCODE_EIS_MODE_EN_BIT  = 0,
};

/*! @struct am_encode_eis_ctrl_s
 *  @brief For eis control GET and SET
 */
/*! @typedef am_encode_eis_ctrl_t
 *  @brief For eis control GET and SET
 */
typedef struct am_encode_eis_ctrl_s {
  /*!
   * @sa AM_ENCODE_EIS_CTRL_BITS
   */
  uint32_t enable_bits;

  /*!
   * 5: eis_full, 4: rotate + pitch, 3: yaw, 2: rotate, 1: pitch, 0: disable
   */
  int32_t eis_mode;
} am_encode_eis_ctrl_t;
/*! @} */ /* End of EIS Control */
/******************************************************************************/

/*
 * bitrate control
 */
/********************************bitrate Control******************************/
/*! @defgroup airapi-datastructure-video-bitrate Control
 *  @ingroup airapi-datastructure-video
 *  @brief bitrate control related parameters used,
 *       refer to @ref airapi-commandid-video-bitrate "bitrate Control Commands"
 *       to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_SET
 *  @{
 */ /* Start of bitrate Control */

/*! @enum AM_BITRATE_BITS
 *  @brief bitrate control type to indicate which config needs to be modified
 */
enum AM_BITRATE_BITS {
  AM_BITRATE_EN_BIT = 0,
};

/*! @struct am_bitrate_s
 *  @brief For bitrate control GET and SET
 */
/*! @typedef am_bitrate_t
 *  @brief For bitrate control GET and SET
 */
typedef struct am_bitrate_s {
  /*!
   * @sa AM_BITRATE_BITS
   */
  uint32_t enable_bits;

  uint32_t stream_id;

  uint32_t rate_control_mode;
  int32_t target_bitrate;
} am_bitrate_t;
/*! @} */ /* End of bitrate Control */
/******************************************************************************/

/*
 * stream framefactor control
 */
/***************************stream framefactor Control*************************/
/*! @defgroup airapi-datastructure-video-bitrate Control
 *  @ingroup airapi-datastructure-video
 *  @brief framefactor control related parameters used,
 *         refer to @ref airapi-commandid-video-framefactor
 *         "framefactor Control Commands" to see related commands.
 *
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_SET
 *  @{
 */ /* Start of framefactor Control */

/*! @enum AM_FRAMEFACTOR_BITS
 *  @brief framefactor control type indicate which config needs to be modified
 */
enum AM_FRAMEFACTOR_BITS {
  AM_FRAMEFACTOR_EN_BIT = 0,
};

/*! @struct am_framefactor_s
 *  @brief For framefactor control GET and SET
 */
/*! @typedef am_framefactor_t
 *  @brief For framefactor control GET and SET
 */
typedef struct am_framefactor_s {
  /*!
   * @sa AM_FRAMEFACTOR_BITS
   */
  uint32_t enable_bits;

  uint32_t stream_id;

  uint32_t mul;
  uint32_t div;
} am_framefactor_t;
/*! @} */ /* End of framefactor Control */
/******************************************************************************/


/*
 * Overlay Deprecated
 */
/*****************************Overlay Deprecated*******************************/
/*! @defgroup airapi-datastructure-video-overlay-deprecated Video Overlay Deprecated
 *  @ingroup airapi-datastruture-video
 *  @brief Overlay deprecated related parameters used,
 *         refer to @ref airapi-commandid-video-overlay-deprecated "Overlay
 *         Deprecated Commands" to see related commands.
 *  @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
 *  @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 *  @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
 *  @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 *  @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 *  @{
 */ /* Start of OVERLAY Deprecated Control */

/*! @enum AM_OVERLAY_DEPRECATED_BITS
 *  @brief OVERLAY Deprecated control type to indicate which config needs to be modified
 */
enum AM_OVERLAY_DEPRECATED_BITS {
  AM_REMOVE_EN_BIT = 0,           //!< remove overlay
  AM_ENABLE_EN_BIT = 1,           //!< enable overlay
  AM_DISABLE_EN_BIT = 2,          //!< disable overlay
  AM_ADD_EN_BIT = 3,              //!< add overlay
  AM_WIDTH_EN_BIT = 4,            //!< overlay area width for add
  AM_HEIGHT_EN_BIT = 5,           //!< overlay area height for add
  AM_LAYOUT_EN_BIT = 6,           //!< overlay area layout for add
  AM_OFFSETX_EN_BIT = 7,          //!< overlay area start x for add
  AM_OFFSETY_EN_BIT = 8,          //!< overlay area start y for add
  AM_TYPE_EN_BIT = 9,             //!< overlay area overlay type for add
  AM_ROTATE_EN_BIT = 10,          //!< overlay area rotate attribute for add
  AM_FONT_TYPE_EN_BIT = 11,       //!< font type if overlay type is string for add
  AM_FONT_SIZE_EN_BIT = 12,       //!< font size if overlay type is string for add
  AM_FONT_COLOR_EN_BIT = 13,      //!< font color if overlay type is string for add
  AM_FONT_OUTLINE_W_EN_BIT = 14,  //!< font outline width if overlay type is string for add
  AM_FONT_HOR_BOLD_EN_BIT = 15,   //!< font hor_bold if overlay type is string for add
  AM_FONT_VER_BOLD_EN_BIT = 16,   //!< font ver_bold if overlay type is string for add
  AM_FONT_ITALIC_EN_BIT = 17,     //!< font italic if overlay type is string for add
  AM_STRING_EN_BIT = 18,          //!< string if overlay type is string for add
  AM_COLOR_KEY_EN_BIT = 19,       //!< color used to transparent if overlay type is picture for add
  AM_COLOR_RANGE_EN_BIT = 20,     //!< color range used to transparent with color key for add
  AM_BMP_EN_BIT = 21,             //!< bmp file path if overlay type is picture for add
  AM_BMP_AREA_INIT_EN_BIT = 22,   //!< init a area for bmp type overlay
  AM_BMP_AREA_ADD_EN_BIT = 23,    //!< add a bmp file to the area which init before
  AM_BMP_AREA_RENDER_EN_BIT = 24, //!< render the area which be added many bmp files to display
  AM_BMP_AREA_UPDATE_EN_BIT = 25, //!< update a bmp file for the area which is init and render before
  AM_UPDATE_EN_BIT = 26,          //!< update overlay
  AM_CHAR_SPACING_EN_BIT = 27,    //!< char spacing
};


/*! @macros OSD_MAX_STRING
 *  @brief OVERLAY Deprecated max char number which overlay type is string
 */
#define OSD_MAX_STRING    (256)  //!<just for test, if it's big size, the total size maybe large than
                                //!<AM_MAX_IPC_MESSAGE_SIZE which may cause AIR API call failed

/*! @macros OSD_MAX_FILENAME
  *  @brief OVERLAY Deprecated max size number of bmp file and font file full name
 */
#define OSD_MAX_FILENAME  (128)  //!<just for test, if it's big size, the total size maybe large than
                                //!<AM_MAX_IPC_MESSAGE_SIZE which may cause AIR API call failed

/*! @struct am_overlay_deprecated_id_s
 *  @brief For overlay deprecated Get input parameter and SET
 */
/*! @typedef am_overlay_deprecated_id_t
 *  @brief For overlay deprecated Get input parameter and SET
 */
typedef struct am_overlay_deprecated_id_s {
    /*!
     * @sa AM_OVERLAY_DEPRECATED_BITS
     */
    uint32_t enable_bits;

    uint32_t stream_id;
    uint32_t area_id;
} am_overlay_deprecated_id_t;

/*! @struct am_overlay_deprecated_set_s
 *  @brief For Overlay deprecated SET
 */
/*! @typedef am_overlay_deprecated_set_t
 *  @brief For Overlay deprecated SET
 */
typedef struct am_overlay_deprecated_set_s {
    /*!
     * @sa AM_OVERLAY_DEPRECATED_BITS
     */
    uint32_t enable_bits;

    /*!
     * overlay area to manipulate
     */
    am_overlay_deprecated_id_t osd_id;

} am_overlay_deprecated_set_t;

/*! @struct am_overlay_deprecated_area_s
 *  @brief For overlay deprecated GET and ADD
 */
/*! @typedef am_overlay_deprecated_area_t
 *  @brief For overlay deprecated GET and ADD
 */
typedef struct am_overlay_deprecated_area_s {
    /*!
     * whether enable area when add, when used in GET,
     * 0 = disable, 1 = enable, other value = not created
     */
    uint32_t enable;

    /*!
     * area width to add
     */
    uint32_t width;

    /*!
     * area height to add
     */
    uint32_t height;

    /*!
     * area layout to add, 0:left top; 1:right top; 2:left bottom;
     * 3:right bottom; 4:manual set by offset_x and offset_y
     */
    uint32_t layout;

    /*!
     * area offset x to add
     */
    uint32_t offset_x;

    /*!
     * area offset y to add
     */
    uint32_t offset_y;

    /*!
     * area type to add, 0:generic string; 1:string time;
     * 2:bmp picture;3:test pattern
     */
    uint32_t type;

    /*!
     * area rotate attribute to add
     */
    uint32_t  rotate;

    /*!
     * text font type name
     */
    char font_type[OSD_MAX_FILENAME];

    /*!
     * string font size
     */
    uint32_t font_size;

    /*!
     * string font color0-7 is predefine color: 0,white;1,black;2,red;3,blue;
     * 4,green;5,yellow;6,cyan;7,magenta; >7,user custom color(VUYA format)
     */
    uint32_t font_color;

    /*!
     * string font outline width
     */
    uint32_t font_outline_w;

    /*!
     * string font hor bold size
     */
    uint32_t font_hor_bold;

    /*!
     * string font ver bold size
     */
    uint32_t font_ver_bold;

    /*!
     * string font italic
     */
    uint32_t font_italic;

    /*!
     * char spacing
     */
    uint32_t spacing;

    /*!
     * color used to transparent in picture type(VUYA format):
     * v:bit[24-31],u:bit[16-23],y:bit[8-15],a:bit[0-7]
     */
    uint32_t color_key;

    /*!
     * color range used to transparent with color_key
     */
    uint32_t color_range;

    /*!
     * string to be insert
     */
    char str[OSD_MAX_STRING];

    /*!
     * bmp full path name to be insert
     */
    char bmp[OSD_MAX_FILENAME];
} am_overlay_deprecated_area_t;

/*! @struct am_overlay_deprecated_s
 *  @brief For overlay deprecated ADD
 */
/*! @typedef am_overlay_deprecated_t
 *  @brief For overlay deprecated ADD
 */
typedef struct am_overlay_deprecated_s {
    /*!
     * @sa AM_OVERLAY_DEPRECATED_BITS
     */
    uint32_t enable_bits;

    uint32_t stream_id;
    /*!
     * osd area parameter(add of SET and GET)
     */
    am_overlay_deprecated_area_t area;
} am_overlay_deprecated_t;

/*! @struct am_overlay_deprecated_area_update_s
 *  @brief For overlay deprecated area UPDATE
 */
/*! @typedef am_overlay_deprecated_area_update_t
 *  @brief For overlay deprecated area UPDATE
 */
typedef struct am_overlay_deprecated_area_update_s {
    /*!
     * @sa AM_OVERLAY_DEPRECATED_BITS
     */
    uint32_t enable_bits;

    /*!
     * encode stream id
     */
    uint32_t stream_id;

    /*!
     * overlay area id
     */
    uint32_t area_id;

    /*!
     * string to be update
     */
    char str[OSD_MAX_STRING];

    /*!
     * bmp full path name to be update
     */
    char bmp[OSD_MAX_FILENAME];
} am_overlay_deprecated_area_update_t;

/*! @struct am_bmp_overlay_deprecated_area_s
 *  @brief For BMP type overlay deprecated area INIT/ADD/UPDATE
 */
/*! @typedef am_bmp_overlay_deprecated_area_t
 *  @brief For BMP type overlay deprecated area INIT/ADD/UPDATE
 */
typedef struct am_bmp_overlay_deprecated_area_s {
    /*!
     * @sa AM_OVERLAY_DEPRECATED_BITS
     */
    uint32_t enable_bits;

    /*!
     * encode stream id
     */
    uint32_t stream_id;

    /*!
     * overlay area id for add/update
     */
    uint32_t area_id;

    /*!
     * area width for init
     */
    uint32_t width;

    /*!
     * area height for init
     */
    uint32_t height;

    /*!
     * area offset x for init/add/update
     */
    uint32_t offset_x;

    /*!
     * area offset y for init/add/update
     */
    uint32_t offset_y;

    /*!
     * color used to transparent for add/update:
     * v:bit[24-31],u:bit[16-23],y:bit[8-15],a:bit[0-7],
     */
    uint32_t color_key;

    /*!
     * color range used to transparent with color_key for add/update
     */
    uint32_t color_range;

    /*!
     * bmp full path name for add/update
     */
    char bmp[OSD_MAX_FILENAME];

} am_bmp_overlay_deprecated_area_t;

/*! @} */ /* End of Overlay Deprecated */
/******************************************************************************/

/*
 * Overlay
 */
/************************************Overlay***********************************/
/*! @defgroup airapi-datastructure-video-overlay Video Overlay
 *  @ingroup airapi-datastruture-video
 *  @brief Overlay related parameters used,
 *         refer to @ref airapi-commandid-video-overlay "Overlay Commands"
 *         to see related commands.
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 *  @{
 */ /* Start of Overlay Control */

/*! @enum AM_OVERLAY_BITS
 *  @brief Overlay control type to indicate which option needs to be modified
 */
enum AM_OVERLAY_BITS {
  AM_OVERLAY_REMOVE_EN_BIT = 0,          //!< remove a overlay area
  AM_OVERLAY_ENABLE_EN_BIT = 1,          //!< enable a overlay area
  AM_OVERLAY_DISABLE_EN_BIT = 2,         //!< disable a overlay area
  AM_OVERLAY_INIT_EN_BIT = 3,            //!< init a area
  AM_OVERLAY_DATA_ADD_EN_BIT = 4,        //!< add a data block to area
  AM_OVERLAY_DATA_UPDATE_EN_BIT = 5,     //!< update a data block for a area
  AM_OVERLAY_DATA_REMOVE_EN_BIT = 6,     //!< remove a data block for a area
  AM_OVERLAY_ROTATE_EN_BIT = 7,          //!< area whether rotated with stream
  AM_OVERLAY_BG_COLOR_EN_BIT = 8,        //!< area or text background color
  AM_OVERLAY_BUF_NUM_EN_BIT = 9,         //!< area buffer number
  AM_OVERLAY_RECT_EN_BIT = 10,           //!< area or data block size and offset
  AM_OVERLAY_DATA_TYPE_EN_BIT = 11,      //!< data block type
  AM_OVERLAY_STRING_EN_BIT = 12,         //!< string to be add or update
  AM_OVERLAY_TIME_EN_BIT = 13,           //!< prefix/suffix string or enable msec
                                         //!< display for time type data block
  AM_OVERLAY_CHAR_SPACING_EN_BIT = 14,   //!< char spacing for text
  AM_OVERLAY_FONT_TYPE_EN_BIT = 15,      //!< font type for text data type
  AM_OVERLAY_FONT_SIZE_EN_BIT = 16,      //!< font size for text data type
  AM_OVERLAY_FONT_COLOR_EN_BIT = 17,     //!< font color for text data type
  AM_OVERLAY_FONT_OUTLINE_EN_BIT = 18,   //!< font outline for text data type
  AM_OVERLAY_FONT_BOLD_EN_BIT = 19,      //!< font hor and ver bold
  AM_OVERLAY_FONT_ITALIC_EN_BIT = 20,    //!< font italic for text data type
  AM_OVERLAY_BMP_EN_BIT = 21,            //!< bmp file to be add or update
  AM_OVERLAY_BMP_COLOR_EN_BIT = 22,      //!< color key and range used for transparent
  AM_OVERLAY_ANIMATION_EN_BIT = 23,      //!< animation parameter
};


/*! @macros OVERLAY_MAX_STRING
 *  @brief Overlay max char number which overlay type is string
 */
#define OVERLAY_MAX_STRING    (128) //!< just for test, if it's big size, the
                                    //!< total size maybe large than
                                    //!< AM_MAX_IPC_MESSAGE_SIZE which may cause
                                    //!< AIR API call failed

/*! @macros OVERLAY_MAX_FILENAME
  *  @brief Overlay max size number of bmp file and font file full name
 */
#define OVERLAY_MAX_FILENAME  (128) //!< just for test, if it's big size, the
                                    //!< total size maybe large than
                                    //!< AM_MAX_IPC_MESSAGE_SIZE which may cause
                                    //!< AIR API call failed

/*! @struct am_overlay_id_s
 *  @brief For overlay area SET/GET and data GET
 */
/*! @typedef am_overlay_id_t
 *  @brief For overlay area SET/GET and data GET
 */
typedef struct am_overlay_id_s {
    /*!
     * @sa AM_OVERLAY_BITS
     */
    uint32_t enable_bits;

    /*!
     * encode stream id
     */
    uint32_t stream_id;

    /*!
     * area id in stream
     */
    uint32_t area_id;

    /*!
     * data block index in area
     */
    uint32_t data_index;

} am_overlay_id_t;

/*! @struct am_overlay_limit_val_s
 *  @brief For get overlay platform and user defined limit value
 */
/*! @typedef am_overlay_limit_val_t
 *  @brief For get overlay platform and user defined limit value
 */
typedef struct am_overlay_limit_val_s {
    /*!
     * platform support max encode stream number
     */
    uint32_t platform_stream_num_max;

    /*!
     * platform support max overlay area number per stream
     */
    uint32_t platform_overlay_area_num_max;

    /*!
     * user defined max stream number for overlay
     */
    uint32_t user_def_stream_num_max;

    /*!
     * user defined max overlay area number per stream
     */
    uint32_t user_def_overlay_area_num_max;

} am_overlay_limit_val_t;

/*! @struct am_overlay_data_s
 *  @brief For overlay data ADD/UPDATE/GET
 */
/*! @typedef am_overlay_data_t
 *  @brief For overlay data ADD/UPDATE/GET
 */
typedef struct am_overlay_data_s {
    /*!
     * @sa am_overlay_id_s
     */
    am_overlay_id_t id;

    /*!
     * @sa AM_OVERLAY_BITS
     */
    uint32_t enable_bits;

    /*!
     * data block width
     */
    uint32_t width;

    /*!
     * data block height
     */
    uint32_t height;

    /*!
     * data block offset x in the area
     */
    uint32_t offset_x;

    /*!
     * data block offset y in the area
     */
    uint32_t offset_y;

    /*!
     * data block type, 0:text; 1:picture; 2:time(a special text type)
     */
    uint32_t type;

    /*!
     * char spacing
     */
    uint32_t spacing;

    /*!
     * text background color
     */
    uint32_t bg_color;

    /*!
     * font size
     */
    uint32_t font_size;

    /*!
     * font color0-7 is predefine color: 0,white;1,black;2,red;3,blue;
     * 4,green;5,yellow;6,cyan;7,magenta; >7,user custom color(VUYA format)
     */
    uint32_t font_color;

    /*!
     * font outline width
     */
    uint32_t font_outline_w;

    /*!
     * font outline color
     */
    uint32_t font_outline_color;

    /*!
     * font hor bold size
     */
    uint32_t font_hor_bold;

    /*!
     * font ver bold size
     */
    uint32_t font_ver_bold;

    /*!
     * font italic
     */
    uint32_t font_italic;

    /*!
     * whether enable msec display when type is time
     */
    uint32_t msec_en;

    /*!
     * bmp number in the animation file
     */
    uint32_t bmp_num;

    /*!
     * frame interval to update bmp when type is animation
     */
    uint32_t interval;

    /*!
     * color used to transparent in picture type(VUYA format):
     * v:bit[24-31],u:bit[16-23],y:bit[8-15],a:bit[0-7]
     */
    uint32_t color_key;

    /*!
     * color range used to transparent with color_key
     */
    uint32_t color_range;

    /*!
     * string to be insert
     */
    char str[OVERLAY_MAX_STRING];

    /*!
     * prefix string to be insert when type is time
     */
    char pre_str[OVERLAY_MAX_STRING];

    /*!
     * suffix string to be insert when type is time
     */
    char suf_str[OVERLAY_MAX_STRING];

    /*!
     * font type name
     */
    char font_type[OVERLAY_MAX_FILENAME];

    /*!
     * bmp full path name to be insert
     */
    char bmp[OVERLAY_MAX_FILENAME];
} am_overlay_data_t;

/*! @struct am_overlay_area_s
 *  @brief For Overlay area INIT and GET
 */
/*! @typedef am_overlay_area_t
 *  @brief For Overlay area INIT and GET
 */
typedef struct am_overlay_area_s {
    /*!
     * @sa AM_OVERLAY_BITS
     */
    uint32_t enable_bits;

    /*!
     * encode stream id
     */
    uint32_t stream_id;

    /*!
     * area state, 0 = disable, 1 = enable, other value = not created
     */
    uint32_t enable;

    /*!
     * area rotate attribute
     */
    uint32_t  rotate;

    /*!
     * area background color
     */
    uint32_t  bg_color;

    /*!
     * area width
     */
    uint32_t width;

    /*!
     * area height
     */
    uint32_t height;

    /*!
     * area offset x in the stream
     */
    uint32_t offset_x;

    /*!
     * area offset y in the stream
     */
    uint32_t offset_y;

    /*!
     * number of buffer in the area
     */
    uint32_t buf_num;

    /*!
     * number of data block in the area, used for GET
     */
    uint32_t num;
} am_overlay_area_t;

/*! @} */ /* End of Overlay */
/******************************************************************************/
/*! @} */ /* End of defgroup airapi-video */

#endif /* _AM_API_VIDEO_H_ */

/*******************************************************************************
 * am_api_cmd_video.h
 *
 * History:
 *   2015-4-15 - [ypchang] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
/*! @file am_api_cmd_video.h
 *  @brief This file defines Video Service related commands
 */
#ifndef ORYX_INCLUDE_SERVICES_AM_API_CMD_VIDEO_H_
#define ORYX_INCLUDE_SERVICES_AM_API_CMD_VIDEO_H_

#include "commands/am_service_impl.h"

/*! @enum AM_SYS_IPC_MW_CMD_VIDEO
 *  @brief Used for system IPC basic function
 */
enum AM_SYS_IPC_MW_CMD_VIDEO
{
  //! _AM_IPC_MW_CMD_VIDEO_VIN_GET
  _AM_IPC_MW_CMD_VIDEO_VIN_GET = VIDEO_SERVICE_CMD_START,

  //! _AM_IPC_MW_CMD_VIDEO_VIN_SET
  _AM_IPC_MW_CMD_VIDEO_VIN_SET,

  //! _AM_IPC_MW_CMD_VIDEO_VOUT_GET
  _AM_IPC_MW_CMD_VIDEO_VOUT_GET,

  //! _AM_IPC_MW_CMD_VIDEO_VOUT_SET
  _AM_IPC_MW_CMD_VIDEO_VOUT_SET,

  //! _AM_IPC_MW_CMD_VIDEO_STREAM_FMT_GET
  _AM_IPC_MW_CMD_VIDEO_STREAM_FMT_GET,

  //! _AM_IPC_MW_CMD_VIDEO_STREAM_FMT_SET
  _AM_IPC_MW_CMD_VIDEO_STREAM_FMT_SET,

  //! _AM_IPC_MW_CMD_VIDEO_STREAM_CFG_GET
  _AM_IPC_MW_CMD_VIDEO_STREAM_CFG_GET,

  //! _AM_IPC_MW_CMD_VIDEO_STREAM_CFG_SET
  _AM_IPC_MW_CMD_VIDEO_STREAM_CFG_SET,

  //! _AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_GET
  _AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_GET,

  //! _AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_SET
  _AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_SET,

  //! _AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_GET
  _AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_GET,

  //! _AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_SET
  _AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_SET,

  //! _AM_IPC_MW_CMD_VIDEO_STREAM_STATUS_GET
  _AM_IPC_MW_CMD_VIDEO_STREAM_STATUS_GET,

  //! _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_DYNAMIC_SET
  _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_DYNAMIC_SET,

  //! _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_DYNAMIC_GET
  _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_DYNAMIC_GET,

  //! _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_SET
  _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_SET,

  //! _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_GET
  _AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_GET,

  //! _AM_IPC_MW_CMD_VIDEO_ENCODE_STOP
  _AM_IPC_MW_CMD_VIDEO_ENCODE_STOP,

  //! _AM_IPC_MW_CMD_VIDEO_ENCODE_START
  _AM_IPC_MW_CMD_VIDEO_ENCODE_START,

  //! _AM_IPC_MW_CMD_VIDEO_ENCODE_FORCE_IDR
  _AM_IPC_MW_CMD_VIDEO_ENCODE_FORCE_IDR,

  //! _AM_IPC_MW_CMD_VIDEO_OSD_OVERLAY_SET
  _AM_IPC_MW_CMD_VIDEO_OSD_OVERLAY_SET,

  //! _AM_IPC_MW_CMD_VIDEO_WARP_GET
  _AM_IPC_MW_CMD_VIDEO_WARP_GET,

  //! _AM_IPC_MW_CMD_VIDEO_WARP_SET
  _AM_IPC_MW_CMD_VIDEO_WARP_SET,

  //! _AM_IPC_MW_CMD_VIDEO_LDC_GET
  _AM_IPC_MW_CMD_VIDEO_LDC_GET,

  //! _AM_IPC_MW_CMD_VIDEO_LDC_SET
  _AM_IPC_MW_CMD_VIDEO_LDC_SET,

  //! _AM_IPC_MW_CMD_VIDEO_FORCE_IDR
  _AM_IPC_MW_CMD_VIDEO_FORCE_IDR,

  //! _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
  _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM,

  //! _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
  _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY,

  //! _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
  _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE,

  //! _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
  _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD,

  //! _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
  _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET,

  //! _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
  _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET,

  //! _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_INIT
  _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_INIT,

  //! _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_ADD
  _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_ADD,

  //! _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_RENDER
  _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_RENDER,

  //! _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_UPDATE
  _AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_UPDATE,

  //!_AM_IPC_MW_CMD_VIDEO_VIN_STOP
  _AM_IPC_MW_CMD_VIDEO_VIN_STOP,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_SET
  _AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_VIN_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_VIN_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_VIN_SET
  _AM_IPC_MW_CMD_VIDEO_CFG_VIN_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_SET
  _AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_SET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_SET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_SET
  _AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_VOUT_SET
  _AM_IPC_MW_CMD_VIDEO_CFG_VOUT_SET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_VOUT_GET
  _AM_IPC_MW_CMD_VIDEO_CFG_VOUT_GET,

  //!_AM_IPC_MW_CMD_VIDEO_CFG_ALL_LOAD
  _AM_IPC_MW_CMD_VIDEO_CFG_ALL_LOAD,


  //! _AM_IPC_MW_CMD_VIDEO_DYN_FORCE_IDR
  _AM_IPC_MW_CMD_VIDEO_DYN_FORCE_IDR,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_MAX_NUM_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_MAX_NUM_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_BUFFER_MAX_NUM_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_BUFFER_MAX_NUM_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_LBR_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_LBR_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_LBR_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_LBR_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_WARP_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_WARP_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_WARP_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_WARP_SET,

  //!_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_STATUS_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_STREAM_STATUS_GET,

  //! _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_UPDATE
  _AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_UPDATE,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT
  _AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_EIS_SET
  _AM_IPC_MW_CMD_VIDEO_DYN_EIS_SET,

  //! _AM_IPC_MW_CMD_VIDEO_DYN_EIS_GET
  _AM_IPC_MW_CMD_VIDEO_DYN_EIS_GET,
};


/******************************Video Service CMDS******************************/
/*! @defgroup airapi-commandid-video Air API Command IDs - Video Service
 *  @ingroup airapi-commandid
 *  @brief Video Service Related command IDs,
 *         refer to @ref airapi-datastructure-video
 *         "Data Structure of Video Service" to see data structures.
 *  @{
 */

/*! @defgroup airapi-commandid-video-feature feature Commands
 *  @ingroup airapi-commandid-video
 *  @brief feature related Commands,
 *  see @ref airapi-datastructure-video-feature "feature Parameters"
 *  for more information.
 *  @sa AMAPIHelper
 *  @sa am_feature_config_s
 *  @{
 */ /* Start of feature Commands Section */

/*! @brief Get video feature information
 *
 * use this command to get video feature information
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_GET
 * @sa am_feature_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_GET                                    \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_GET,               \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set video feature parameters
 *
 * use this command to set video feature parameters
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_SET
 * @sa am_feature_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_SET                                    \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_FEATURE_SET,               \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of feature Commands */



/*! @defgroup airapi-commandid-video-vin VIN Commands
 *  @ingroup airapi-commandid-video
 *  @brief VIN related Commands,
 *  see @ref airapi-datastructure-video-vin "VIN Parameters"
 *  for more information.
 *  @sa AMAPIHelper
 *  @sa am_vin_config_s
 *  @{
 */ /* Start of VIN Commands Section */

/*! @brief Get video VIN information
 *
 * use this command to get video VIN device information
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_SET
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_STOP
 * @sa am_vin_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_VIN_GET                                            \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_VIN_GET,                       \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set video VIN parameters
 *
 * use this command to set video VIN device parameters
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_GET
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_STOP
 * @sa am_vin_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_VIN_SET                                            \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_VIN_SET,                       \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set video VIN parameters
 *
 * use this command to get video VIN device parameters from config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_VIN_SET
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_STOP
 * @sa am_vin_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_VIN_GET                                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_VIN_GET,                   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set video VIN parameters
 *
 * use this command to set video VIN device parameters to config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_VIN_GET
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_STOP
 * @sa am_vin_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_VIN_SET                                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_VIN_SET,                   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Stop video VIN
 *
 * use this command to stop video VIN device
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_GET
 * @sa AM_IPC_MW_CMD_VIDEO_VIN_SET
 */
#define AM_IPC_MW_CMD_VIDEO_VIN_STOP                                           \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_VIN_STOP,                      \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of VIN Commands */

/*! @defgroup airapi-commandid-video-vout VOUT Commands
 *  @ingroup airapi-commandid-video
 *  @brief VOUT related commands,
 *  see @ref airapi-datastructure-video-vout "VOUT Parameters"
 *  for more information.
 *  @sa AMAPIHelper
 *  @{
 */ /* Start of VOUT Commands Section */

/*! @brief Shutdown a video VOUT
 *
 * use this command to halt a video VOUT device
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_VOUT_SET
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_VOUT_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT                                      \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT,                 \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set video VOUT parameters
 *
 * use this command to set video VOUT device parameters to config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_VOUT_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT
 * @sa am_vout_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_VOUT_SET                                       \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_VOUT_SET,                  \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get video VOUT parameters
 *
 * use this command to get video VOUT device parameters from config file
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_VOUT_HALT
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_VOUT_SET
 * @sa am_vout_config_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_VOUT_GET                                       \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_VOUT_GET,                  \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of VOUT Commands Section*/

/*! @defgroup airapi-commandid-video-src-buf-fmt Source Buffer Format Commands
 *  @ingroup airapi-commandid-video
 *  @brief Source Buffer format related commands,
 *  see @ref airapi-datastructure-video-src-buf-fmt
 *  "Source Buffer Format" for more information.
 *  @sa AMAPIHelper
 *  @sa am_buffer_fmt_s
 *  @{
 */ /* Start of Source Buffer Format */

/*! @brief Get Buffer Format
 *
 * use this command to get buffer format
 * @sa AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_SET
 * @sa am_buffer_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_GET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_GET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set buffer format
 *
 * use this command to set buffer format
 * @sa AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_GET
 * @sa am_buffer_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_SET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BUFFER_FMT_SET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)


/*! @brief Set buffer format
 *
 * use this command to get buffer format from config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_SET
 * @sa am_buffer_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_GET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_GET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set buffer format
 *
 * use this command to set buffer format to config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_GET
 * @sa am_buffer_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_SET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_BUFFER_SET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get buffer max number
 *
 * use this command to get buffer max number
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_BUFFER_MAX_NUM_GET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_BUFFER_MAX_NUM_GET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of Source Buffer Format Commands */

/*! @defgroup airapi-commandid-video-stream-fmt Stream Format Commands
 *  @ingroup airapi-commandid-video
 *  @brief Stream format related Commands,
 *  see @ref airapi-datastructure-video-stream-fmt
 *  "Stream Formate Parameters" for more information.
 *  @sa AMAPIHelper
 *  @sa am_stream_fmt_s
 *  @{
 */ /* Start of Stream Format Parameters */

/*! @brief Get stream format
 *
 * use this command to get stream format
 * @sa AM_IPC_MW_CMD_VIDEO_STREAM_FMT_SET
 * @sa am_stream_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_STREAM_FMT_GET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_STREAM_FMT_GET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream format
 *
 * use this command to set stream format
 * @sa AM_IPC_MW_CMD_VIDEO_STREAM_FMT_GET
 * @sa am_stream_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_STREAM_FMT_SET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_STREAM_FMT_SET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream format
 *
 * use this command to get stream format from config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_SET
 * @sa am_stream_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_GET                                 \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_GET,            \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream format
 *
 * use this command to set stream format to config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_GET
 * @sa am_stream_fmt_s
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_SET                                 \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_FMT_SET,            \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get stream max number
 *
 * use this command to get stream max number
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_MAX_NUM_GET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_MAX_NUM_GET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get stream bitrate dynamically
 *
 * use this command to get stream bitrate dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_GET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_GET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream bitrate dynamically
 *
 * use this command to set stream bitrate dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_SET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_SET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_BITRATE_SET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get stream framerate factor dynamically
 *
 * use this command to get stream framerate factor dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_GET                         \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_GET,    \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream framerate factor dynamically
 *
 * use this command to set stream framerate factor dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_SET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_SET                         \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_FRAMEFACTOR_SET,    \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get mjpeg quality dynamically
 *
 * use this command to get mjpeg quality dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_GET                              \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_GET,         \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set mjpeg quality dynamically
 *
 * use this command to set mjpeg quality dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_SET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_SET                              \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_MJPEG_QUALITY_SET,         \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)


/*! @brief Get h264 gop dynamically
 *
 * use this command to get h264 gop dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_GET                                   \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_GET,              \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set h264 gop dynamically
 *
 * use this command to set h264 gop dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_SET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_SET                                   \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_H264_GOP_SET,              \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get stream offset dynamically
 *
 * use this command to get stream offset dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_GET                              \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_GET,         \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream offset dynamically
 *
 * use this command to set stream offset dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_SET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_SET                              \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_OFFSET_SET,         \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)


/*! @} */ /* End of Stream Format Commands */

/*! @defgroup airapi-commandid-video-stream-cfg Stream Config Commands
 *  @ingroup airapi-commandid-video
 *  @brief Stream config related commands,
 *  see @ref airapi-datastructure-video-stream-cfg "Stream Config Parameters"
 *  for more information.
 *  @sa AMAPIHelper
 *  @sa am_stream_cfg_s
 *  @{
 */ /* Start of Stream Config Commands */

/*! @brief Get stream configuration
 *
 * use this command to get stream configuration
 * @sa AM_IPC_MW_CMD_VIDEO_STREAM_CFG_SET
 * @sa am_stream_cfg_s
 */
#define AM_IPC_MW_CMD_VIDEO_STREAM_CFG_GET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_STREAM_CFG_GET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream configuration
 *
 * use this command to set stream configuration
 * @sa AM_IPC_MW_CMD_VIDEO_STREAM_CFG_GET
 * @sa am_stream_cfg_s
 */
#define AM_IPC_MW_CMD_VIDEO_STREAM_CFG_SET                                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_STREAM_CFG_SET,                \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream configuration
 *
 * use this command to get stream h264 parameters from config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_SET
 * @sa am_h264_cfg_t
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_GET                                \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_GET,           \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream configuration
 *
 * use this command to set stream h264 parameters to config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_GET
 * @sa am_h264_cfg_t
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_SET                                \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H264_SET,           \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream configuration
 *
 * use this command to get stream h265 parameters from config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_SET
 * @sa am_h264_cfg_t
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_GET                                \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_GET,           \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream configuration
 *
 * use this command to set stream h265 parameters to config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_GET
 * @sa am_h264_cfg_t
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_SET                                \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_H265_SET,           \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream configuration
 *
 * use this command to get stream mjpeg parameters from config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_SET
 * @sa am_mjpeg_cfg_t
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_GET                               \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_GET,          \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set stream configuration
 *
 * use this command to set stream mjpeg parameters to config file
 * @sa AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_GET
 * @sa am_mjpeg_cfg_t
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_SET                               \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_STREAM_MJPEG_SET,          \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */ /* End of Stream Config Commands */

/*! @defgroup airapi-commandid-video-buf-style Buffer Allocation Style Commands
 *  @ingroup airapi-commandid-video
 *  @brief Buffer allocation style related commands,
 *  see @ref airapi-datastructure-video-src-buf-style
 *  "Buffer Allocation Style" for more information.
 *  @sa AMAPIHelper
 *  @sa am_buffer_alloc_style_s
 *  @{
 */ /* Start of Buffer Allocation Style */

/*! @brief Get buffer allocation style
 *
 * use this command to get buffer allocation style
 * @sa AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_SET
 * @sa am_buffer_alloc_style_s
 */
#define AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_GET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_GET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set buffer allocation style
 *
 * use this command to set buffer allocation style
 * @sa AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_GET
 * @sa am_buffer_alloc_style_s
 */
#define AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_SET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BUFFER_ALLOC_STYLE_SET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */ /* End of Buffer Allocation Style Commands*/

/*! @defgroup airapi-commandid-video-stream-status Stream Status Commands
 *  @ingroup airapi-commandid-video
 *  @brief Stream status related commands
 *  @sa AMAPIHelper
 *  @sa am_stream_status_s
 *  @{
 */ /* Start of Stream Status Commands */

/*! @brief Get stream status
 *
 * use this command to get stream status
 * @sa am_stream_status_s
 */
#define AM_IPC_MW_CMD_VIDEO_STREAM_STATUS_GET                                  \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_STREAM_STATUS_GET,             \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get stream status
 *
 * use this command to get stream status
 * @sa am_stream_status_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_STREAM_STATUS_GET                              \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_STREAM_STATUS_GET,         \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of Stream Status Commands */

/*! @defgroup airapi-commandid-video-enc-on-off Encode Start and Stop Commands
 *  @ingroup airapi-commandid-video
 *  @brief Encode start and stop related commands
 *  @sa AMAPIHelper
 *  @{
 */ /* Start of Encode Start and Stop Commands */

/*! @brief Start to encode
 *
 * use this command to start encoding
 * @sa AM_IPC_MW_CMD_VIDEO_ENCODE_STOP
 */
#define AM_IPC_MW_CMD_VIDEO_ENCODE_START                                       \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_ENCODE_START,                  \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Stop to encode
 *
 * use this command to stop encoding
 * @sa AM_IPC_MW_CMD_VIDEO_ENCODE_START
 */
#define AM_IPC_MW_CMD_VIDEO_ENCODE_STOP                                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_ENCODE_STOP,                   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */ /* End of Encode Start and Stop Commands */

/*! @defgroup airapi-commandid-video-enc-dynamic-control Encode Dynamic Contorl Commands
 *  @ingroup airapi-commandid-video
 *  @brief Encode dynamic control related commands
 *  @sa AMAPIHelper
 *  @{
 */ /* Start of Encode Dynamic Contorl Commands */

/*! @brief Force encode to idr
 *
 * use this command to force encode to idr
 */
#define AM_IPC_MW_CMD_VIDEO_ENCODE_FORCE_IDR                                   \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_ENCODE_FORCE_IDR,              \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Force encode to idr
 *
 * use this command to force encode to idr
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_FORCE_IDR                                      \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_FORCE_IDR,                 \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of Encode Dynamic Contorl Commands */

/*! @defgroup airapi-commandid-video-dptz DPTZ Commands
 *  @ingroup aorapi-commandid-video
 *  @brief DPTZ related commands used
 *  see @ref airapi-datastructure-video-dptz "DPTZ" for more
 *  information.
 *  @sa AMAPIHelper
 *  @{
 */ /* Start of DPTZ */

/*! @brief Get DPTZ Ratio setting dynamically
 *
 * use this command to get DPTZ setting dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SET
 * @sa am_dptz_ratio_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_GET                                 \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_GET,            \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get DPTZ Size setting dynamically
 *
 * use this command to get DPTZ Size setting dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET
 * @sa am_dptz_size_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_GET                                  \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_GET,             \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set DPTZ setting dynamically
 *
 * use this command to set DPTZ setting dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_SET
 * @sa am_dptz_ratio_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_SET                                 \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_RATIO_SET,            \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set DPTZ Size setting dynamically
 *
 * use this command to set DPTZ Size setting dynamically
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET
 * @sa am_dptz_size_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET                                  \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_DPTZ_SIZE_SET,             \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of DPTZ Commands */

/*! @defgroup airapi-commandid-video-warp Warp Commands
 *  @ingroup aorapi-commandid-video
 *  @brief Warp related commands used
 *  see @ref airapi-datastructure-video-warp "Warp" for more
 *  information.
 *  @sa AMAPIHelper
 *  @sa am_warp_s
 *  @{
 */ /* Start of Warp */
/*! @brief Get Warp setting
 *
 * use this command to get Warp setting
 * @sa AM_IPC_MW_CMD_VIDEO_WARP_GET
 * @sa am_warp_s
 */
#define AM_IPC_MW_CMD_VIDEO_WARP_GET                                           \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_WARP_GET,                      \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get Warp setting dynamically
 *
 * use this command to get Warp setting dynamically
 * @sa _AM_IPC_MW_CMD_VIDEO_DYN_WARP_SET
 * @sa am_warp_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_WARP_GET                                       \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_WARP_GET,                  \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set Warp setting
 *
 * use this command to set Warp setting
 * @sa AM_IPC_MW_CMD_VIDEO_WARP_SET
 * @sa am_warp_s
 */
#define AM_IPC_MW_CMD_VIDEO_WARP_SET                                           \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_WARP_SET,                      \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set Warp setting dynamically
 *
 * use this command to set Warp setting dynamically
 * @sa _AM_IPC_MW_CMD_VIDEO_DYN_WARP_GET
 * @sa am_warp_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_WARP_SET                                       \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_WARP_SET,                  \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */ /* End of Warp Commands*/

/*! @defgroup airapi-commandid-video-lbr LBR Control Commands
 *  @ingroup airapi-commandid-video
 *  @brief lbr control related commands used,
 *  see @ref airapi-datastructure-video-lbr "LBR Control" for more
 *  information.
 *  @sa AMAPIHelper
 *  @sa am_encode_h264_lbr_ctrl_s
 *  @{
 */ /* Start of LBR Control */

 /*! @brief Set LBR setting
  *
  * use this command to set h264 lbr setting
  * @sa AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_GET
  * @sa am_encode_h264_lbr_ctrl_s
  */
#define AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_SET                                \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_SET,           \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set LBR setting
 *
 * use this command to set lbr setting
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_LBR_GET
 * @sa am_encode_h264_lbr_ctrl_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_LBR_SET                                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_LBR_SET,                   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

 /*! @brief Get H264 LBR setting
  *
  * use this command to get h264 lbr setting
  * @sa AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_SET
  * @sa am_encode_h264_lbr_ctrl_s
  */
#define AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_GET                                \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_ENCODE_H264_LBR_GET,           \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get LBR setting
 *
 * use this command to get lbr setting
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_LBR_SET
 * @sa am_encode_h264_lbr_ctrl_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_LBR_GET                                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_LBR_GET,                   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @} */ /* End of H264 LBR Control Commands*/

/*! @defgroup airapi-commandid-video-evt-get Event Get Commands
 *  @ingroup airapi-commandid-video
 *  @brief Event related commands,
 *  see @ref airapi-commandid-event "Air API Command IDs - Event Service" for
 *  Event Service related command IDs
 *  @sa AMAPIHelper
 *  @{
 */ /* Start of Event Get*/

/*! @brief Get event
 *
 * use this command to get event
 */
#define AM_IPC_MW_CMD_VIDEO_GET_EVENT                                          \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_GET_EVENT,                     \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NO_NEED_RETURN,                              \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */   /* End of Event Get Commands*/

/*! @defgroup airapi-commandid-video-overlay-deprecated Overlay Deprecated Commands
 *  @ingroup airapi-commandid-video
 *  @brief OVERLAY deprecated related commands,
 *  see @ref airapi-datastructure-video-overlay-deprecated "OVERLAY Deprecated
 *  Parameters" for more information.
 *  @sa AMAPIHelper
 *  @sa am_overlay_deprecated_set_s
 *  @sa am_overlay_deprecated_area_s
 *  @sa am_overlay_deprecated_s
 *  @sa am_bmp_overlay_deprecated_area_s
 *  @{
 */ /* Start of OVERLAY DEPRECATED Commands */

/*! @brief Get overlay max area number
 *
 * use this command to get overlay max area number
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
 */
#define AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM                     \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM,\
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Destroy all overlay
 *
 * use this command to destroy all overlay
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
 */
#define AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY                         \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY,    \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Save all overlay parameter to configure file
 *
 * use this command to save all user parameters to configure file
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
 */
#define AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE                            \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE,       \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Add overlay
 *
 * use this command to add one overlay
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
 * @sa am_overlay_deprecated_s
 */
#define AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set overlay
 *
 * use this command to remove/enable/disable overlay
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
 * @sa am_overlay_deprecated_set_s
 */
#define AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Update overlay
 *
 * use this command to update overlay
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET
 * @sa am_overlay_deprecated_area_update_s
 */
#define AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_UPDATE                          \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_UPDATE,     \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get overlay configure
 *
 * use this command to get all overlay parameters
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa am_overlay_deprecated_area_s
 */
#define AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET                             \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET,        \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Init BMP overlay area
 *
 * use this command to init a BMP type overlay area
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_BMP_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_AREA_RENDER
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_BMP_UPDATE
 * @sa am_bmp_overlay_deprecated_area_s
 */
#define AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_INIT                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_INIT,   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Add BMP to overlay area
 *
 * use this command to add a BMP to a overlay area which had init before
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_AREA_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_AREA_RENDER
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_BMP_UPDATE
 * @sa am_bmp_overlay_deprecated_area_s
 */
#define AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_ADD                         \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_ADD,    \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Render overlay area
 *
 * use this command to render the overlay area when all BMPs have been
 * added to this area
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_AREA_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_BMP_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_BMP_UPDATE
 * @sa am_overlay_id_s
 */
#define AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_RENDER                      \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_RENDER, \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Update overlay area's BMP
 *
 * use this command to update a BMP for the overlay area
 * which had init or render before
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_GET_MAX_NUM
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_OVERLAY_DEPRECATED_SET
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_AREA_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_BMP_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_AREA_RENDER
 * @sa am_overlay_deprecated_id_s
 */
#define AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_UPDATE                      \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_BMP_OVERLAY_DEPRECATED_UPDATE, \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */ /* End of OVERLAY Deprecated Commands */

/*! @defgroup airapi-commandid-video-overlay Overlay Commands
 *  @ingroup airapi-commandid-video
 *  @brief Overlay related commands,
 *  see @ref airapi-datastructure-video-overlay "Overlay Parameters"
 *  for more information.
 *  @sa AMAPIHelper
 *  @sa am_overlay_area_s
 *  @sa am_overlay_data_s
 *  @sa am_overlay_id_s
 *  @sa am_overlay_limit_val_s
 *  @{
 */ /* Start of Overlay Commands */

/*! @brief Get overlay platform and user defined limit value
 *
 * use this command to get overlay max area number
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET                            \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET,       \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Destroy all overlay
 *
 * use this command to destroy all overlay areas
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY                                \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY,           \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Save all overlay parameters to configure file
 *
 * use this command to save all user parameters to configure file
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE                                   \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE,              \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Init a overlay area
 *
 * use this command to init a overlay area
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 * @sa am_overlay_area_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT                                   \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT,              \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Add a data block to area
 *
 * use this command to add a data block to area
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 * @sa am_overlay_data_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD                               \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD,          \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Update a area data block
 *
 * use this command to add one overlay
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 * @sa am_overlay_id_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE                            \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE,       \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Set overlay
 *
 * use this command to remove/enable/disable a area or remove a data block
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 * @sa am_overlay_id_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET                                    \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET,               \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get area parameter
 *
 * use this command to get a area attribute parameter
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET
 * @sa am_overlay_id_s
 * @sa am_overlay_area_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET                                    \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET,               \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get data block parameter
 *
 * use this command to get a data block parameter
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_MAX_NUM_GET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DESTROY
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SAVE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_INIT
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_ADD
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_UPDATE
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_SET
 * @sa AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_GET
 * @sa am_overlay_id_s
 * @sa am_overlay_data_s
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET                               \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_OVERLAY_DATA_GET,          \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */ /* End of Overlay Commands */

/*! @defgroup airapi-commandid-video-eis EIS Config Commands
 *  @ingroup airapi-commandid-video
 *  @brief EIS Control related commands
 *  @sa AMAPIHelper
 *  @sa am_encode_eis_ctrl_s
 *  @{
 */ /* Start of EIS Config Commands */

/*! @brief Set EIS setting
 *
 * use this command to set EIS setting
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_EIS_SET                                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_EIS_SET,                   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)

/*! @brief Get EIS setting
 *
 * use this command to get EIS setting
 */
#define AM_IPC_MW_CMD_VIDEO_DYN_EIS_GET                                        \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_DYN_EIS_GET,                   \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)
/*! @} */ /* End of EIS Config Commands */

/*! @defgroup airapi-commandid-video-config-load Load Config Commands
 *  @ingroup airapi-commandid-video
 *  @brief  Load module config related commands
 *  @sa AMAPIHelper
 *  @{
 */ /* Start of CONFIG LOAD Commands Section */

/*! @brief Load all modules config
 *
 * use this command to load all modules config file
 */
#define AM_IPC_MW_CMD_VIDEO_CFG_ALL_LOAD                                       \
BUILD_IPC_MSG_ID_WITH_TYPE(_AM_IPC_MW_CMD_VIDEO_CFG_ALL_LOAD,                  \
                           AM_IPC_DIRECTION_DOWN,                              \
                           AM_IPC_NEED_RETURN,                                 \
                           AM_SERVICE_TYPE_VIDEO)


/*! @} */ /* End of CONFIG LOAD Commands Section*/

/*! * @} */ /* End of Video Service Command IDs */
/******************************************************************************/

/*! @example test_video_service.cpp
 *  This is the example program of Video Service APIs.
 */

#endif /* ORYX_INCLUDE_SERVICES_AM_API_CMD_VIDEO_H_ */

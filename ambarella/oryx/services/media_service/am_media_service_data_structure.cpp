/*******************************************************************************
 * am_media_service_data_structure.cpp
 *
 * History:
 *   May 13, 2015 - [ccjing] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#include "am_base_include.h"
#include "am_log.h"
#include "am_media_service_data_structure.h"

AMIApiPlaybackAudioFileList* AMIApiPlaybackAudioFileList::create()
{
  AMApiPlaybackAudioFileList* m_list = new AMApiPlaybackAudioFileList();
  return m_list ? (AMIApiPlaybackAudioFileList*)m_list : NULL;
}

AMIApiPlaybackAudioFileList* AMIApiPlaybackAudioFileList::create(
    AMIApiPlaybackAudioFileList* audio_file)
{
  AMApiPlaybackAudioFileList* m_list = new AMApiPlaybackAudioFileList(audio_file);
  return m_list ? (AMIApiPlaybackAudioFileList*)m_list : NULL;
}

AMApiPlaybackAudioFileList::AMApiPlaybackAudioFileList()
{}

AMApiPlaybackAudioFileList::AMApiPlaybackAudioFileList(
    AMIApiPlaybackAudioFileList* audio_file)
{
  m_list.file_number = audio_file->get_file_number();
  for (uint32_t i = 0; i < m_list.file_number; ++ i) {
    memcpy(m_list.file_list[i],  audio_file->get_file(i).c_str(),
           sizeof(m_list.file_list[i]));
  }
}

AMApiPlaybackAudioFileList::~AMApiPlaybackAudioFileList()
{
  m_list.file_number = 0;
}

bool AMApiPlaybackAudioFileList::add_file(const std::string &file_name)
{
  bool ret = true;
  if(file_name.empty()) {
    ERROR("file_name is empty, can not add it to file list, drop it.");
  } else if (m_list.file_number >= AudioFileList::MAX_FILE_NUMBER) {
    ERROR("This file list is full, the max file number is %d, "
        "please create a new file list.", AudioFileList::MAX_FILE_NUMBER);
    ret = false;
  } else if (file_name.size() >= AudioFileList::MAX_FILENAME_LENGTH) {
    ERROR("file name is too length, the max file name length is %d",
          AudioFileList::MAX_FILENAME_LENGTH);
    ret = false;
  } else {
    memcpy(m_list.file_list[m_list.file_number], file_name.c_str(),
            file_name.size());
    ++ m_list.file_number;
  }
  return ret;
}

std::string AMApiPlaybackAudioFileList::get_file(uint32_t file_number)
{
  if (file_number > 0 && file_number <= m_list.file_number) {
    return std::string(m_list.file_list[file_number - 1]);
  } else {
    WARN("Do not have this file, return a empty string");
    return std::string("");
  }
}

uint32_t AMApiPlaybackAudioFileList::get_file_number()
{
  return m_list.file_number;
}

bool AMApiPlaybackAudioFileList::is_full()
{
  return (m_list.file_number >= AudioFileList::MAX_FILE_NUMBER) ? true : false;
}

char* AMApiPlaybackAudioFileList::get_file_list()
{
  return (char*)&m_list;
}

uint32_t AMApiPlaybackAudioFileList::get_file_list_size()
{
  return sizeof(AudioFileList);
}

void AMApiPlaybackAudioFileList::clear_file()
{
  for (uint32_t i = 0; i < AudioFileList::MAX_FILE_NUMBER; ++ i) {
    memset(m_list.file_list[i], 0, sizeof(m_list.file_list[i]));
  }
  m_list.file_number = 0;
}

void AMApiMediaEvent::set_attr_mjpeg()
{
  m_event.attr = AM_EVENT_MJPEG;
}

bool AMApiMediaEvent::is_attr_mjpeg()
{
  return (m_event.attr == AM_EVENT_MJPEG) ? true : false;
}

void AMApiMediaEvent::set_attr_h26X()
{
  m_event.attr = AM_EVENT_H26X;
}

bool AMApiMediaEvent::is_attr_h26X()
{
  return (m_event.attr = AM_EVENT_H26X) ? true : false;
}

bool AMApiMediaEvent::set_event_id(uint32_t event_id)
{
  bool ret = true;
  if (event_id >= 0) {
    m_event.event_id = event_id;
  } else {
    ret = false;
  }
  return ret;
}

uint32_t AMApiMediaEvent::get_event_id()
{
  return m_event.event_id;
}

bool AMApiMediaEvent::set_pre_cur_pts_num(uint8_t num)
{
  bool ret = true;
  if (num >= 0) {
    m_event.pre_cur_pts_num = num;
  } else {
    ret = false;
  }
  return ret;
}

uint8_t AMApiMediaEvent::get_pre_cur_pts_num()
{
  return m_event.pre_cur_pts_num;
}

bool AMApiMediaEvent::set_after_cur_pts_num(uint8_t num)
{
  bool ret = true;
  if (num >= 0) {
    m_event.after_cur_pts_num = num;
  } else {
    ret = false;
  }
  return ret;
}

uint8_t AMApiMediaEvent::get_after_cur_pts_num()
{
  return m_event.after_cur_pts_num;
}

bool AMApiMediaEvent::set_closest_cur_pts_num(uint8_t num)
{
  bool ret = true;
  if (num >= 0) {
    m_event.closest_cur_pts_num = num;
  } else {
    ret = false;
  }
  return ret;
}

uint8_t AMApiMediaEvent::get_closest_cur_pts_num()
{
  return m_event.closest_cur_pts_num;
}

char* AMApiMediaEvent::get_data()
{
  return (char*)(&m_event);
}

uint32_t AMApiMediaEvent::get_data_size()
{
  return sizeof(m_event);
}

AMIApiMediaEvent* AMIApiMediaEvent::create()
{
  AMApiMediaEvent* result = new AMApiMediaEvent();
  return result ? (AMIApiMediaEvent*)result : nullptr;
}

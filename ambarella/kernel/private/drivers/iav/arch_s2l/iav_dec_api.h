/*
 * iav_dec_api.h
 *
 * History:
 *	2015/01/21 - [Zhi He] created file
 *
 *
 * Copyright (c) 2015 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef __IAV_DEC_API_H__
#define __IAV_DEC_API_H__

//from dsp
typedef enum {
	HDEC_OPM_IDLE = 0,
	HDEC_OPM_VDEC_RUN,
	HDEC_OPM_VDEC_IDLE, // still have last picture left
	HDEC_OPM_VDEC_2_IDLE,
	HDEC_OPM_VDEC_2_VDEC_IDLE,
	HDEC_OPM_JDEC_SLIDE, // must be the first of JPEG stuff
	HDEC_OPM_JDEC_SLIDE_IDLE,
	HDEC_OPM_JDEC_SLIDE_2_IDLE,
	HDEC_OPM_JDEC_SLIDE_2_SLIDE_IDLE,
	HDEC_OPM_JDEC_MULTI_S,
	HDEC_OPM_JDEC_MULTI_S_2_IDLE,
	HDEC_OPM_RAW_DECODE,
	HDEC_OPM_RAW_DECODE_2_IDLE, // not used yet - jrc 12/01/2005
} HDEC_OPMODE;

void iav_clean_decode_stuff(struct ambarella_iav *iav);

int iav_decode_ioctl(struct ambarella_iav *iav, unsigned int cmd, unsigned long arg);

#endif	// __IAV_DEC_API_H__

